This project is a Zend Framework compatible image processing / resizing / manipulation library.

It's a fork of the [Varien Image](http://svn.magentocommerce.com/source/branches/1.7/lib/Varien/) library which is included with Magento.

## Contribution
We are currently looking for people to help us improve this library and add an Imagemagic adapter.

## Standards
All development must conform to [Zend Framework coding standards](http://framework.zend.com/manual/1.12/en/coding-standard.html).